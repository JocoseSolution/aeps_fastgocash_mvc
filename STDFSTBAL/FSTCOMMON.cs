﻿using STDFSTDAL;
using STDFSTSHARED;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STDFSTBAL
{
    public class FSTCOMMON
    {

        public FSTRES GetFSTAmout(FSTREQ fstGo)
        {
            FSTRES OBJRES = new FSTRES();
            try
            {
                FSTCOMMONDAL dal = new FSTCOMMONDAL();
                OBJRES = dal.GetFSTAmout(fstGo);

            }
            catch (Exception ex)
            {

            }
            return OBJRES;
        }

        public Transaction_Webhook_Response Get_TransactionDetail(Transaction_Webhook fstGo)
        {
            Transaction_Webhook_Response OBJRES = new Transaction_Webhook_Response();
            try
            {
                FSTCOMMONDAL dal = new FSTCOMMONDAL();
                OBJRES = dal.Get_TransactionDetail(fstGo);

            }
            catch (Exception ex)
            {

            }
            return OBJRES;
        }
    }
}
