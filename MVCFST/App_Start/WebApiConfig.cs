﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace MVCFST
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.Routes.MapHttpRoute("DefaultApiWithId", "api/{controller}/{id}", new { id = RouteParameter.Optional }, new { id = @"\d+" });
            config.Routes.MapHttpRoute("DefaultApiWithAction", "api/{controller}/{action}");
           

            // Uncomment the following line of code to enable query support for actions with an IQueryable or IQueryable<T> return type.
            // To avoid processing unexpected or malicious queries, use the validation settings on QueryableAttribute to validate incoming queries.
            // For more information, visit http://go.microsoft.com/fwlink/?LinkId=279712.
            //config.EnableQuerySupport();

            // To disable tracing in your application, please comment out or remove the following line of code
            // For more information, refer to: http://www.asp.net/web-api
            //config.EnableSystemDiagnosticsTracing();

            //System.Net.Http.Formatting.MediaTypeFormatterCollection fd = new System.Net.Http.Formatting.MediaTypeFormatterCollection();
            //config.Formatters.Add(fd.JsonFormatter);
            //config.Formatters.Add(fd.XmlFormatter);
            //config.Formatters.XmlFormatter.UseXmlSerializer = true;
            //config.Formatters.JsonFormatter.UseDataContractJsonSerializer = true;

            ////var json = config.Formatters.JsonFormatter;
            ////json.UseDataContractJsonSerializer = true;

            //var xml = GlobalConfiguration.Configuration.Formatters.XmlFormatter;
            //xml.UseXmlSerializer = true;
            ////config.MessageHandlers.Insert(0, new ServerCompressionHandler(new GZipCompressor(), new DeflateCompressor()));
            ////Add this configuration for validate input request
            //////config.Filters.Add(new CustomAuthorizeAttribute());
            //////config.Filters.Add(new ValidateModelAttribute());
            //config.Filters.Add(new ValidationExceptionFilterAttribute());
            ////config.EnableCors();
            //GlobalConfiguration.Configuration.Formatters.Insert(0, new TextMediaTypeFormatter());


            //Enable this when want to exclude null values in the JSON output
            //var json = config.Formatters.JsonFormatter;
            //json.SerializerSettings.Formatting = Newtonsoft.Json.Formatting.Indented;
            //json.SerializerSettings.NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore;

            //config.Formatters.Add(json);

            // added to the web api configuration in the application setup
            //var constraintResolver = new DefaultInlineConstraintResolver()
            //{
            //    ConstraintMap =
            //        {
            //            ["apiVersion"] = typeof( ApiVersionRouteConstraint )
            //        }
            //};

            //config.MapHttpAttributeRoutes(constraintResolver);
            //config.AddApiVersioning();



            System.Net.Http.Formatting.MediaTypeFormatterCollection fd = new System.Net.Http.Formatting.MediaTypeFormatterCollection();
            //config.Formatters.Add(fd.JsonFormatter);          
            config.Formatters.Add(fd.XmlFormatter);
            //config.Formatters.XmlFormatter.MediaTypeMappings.Add(oo);
            config.Formatters.XmlFormatter.UseXmlSerializer = true;

            //  config.MessageHandlers.Insert(0, new ServerCompressionHandler(new GZipCompressor(), new DeflateCompressor()));
            //Add this configuration for validate input request
            //config.Filters.Add(new CustomAuthorizeAttribute());
          //  config.Filters.Add(new ValidateModelAttribute());
          //  config.Filters.Add(new ValidationExceptionFilterAttribute());
          //  config.EnableCors();
           // GlobalConfiguration.Configuration.Formatters.Insert(0, new TextMediaTypeFormatter());
            // Web API configuration and services

            // Web API routes
            //config.MapHttpAttributeRoutes();

            //config.Routes.MapHttpRoute(
            //    name: "DefaultApi",
            //    routeTemplate: "api/{controller}/{id}",
            //    defaults: new { id = RouteParameter.Optional }
            //);
        }
    }
}
