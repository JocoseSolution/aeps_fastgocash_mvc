﻿using STDFSTBAL;
using STDFSTSHARED;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MVCFST.Controllers
{
    public class FSTAPIController : ApiController
    {
        [HttpPost, ActionName("GetFSTAmout")]
        public FSTRES GetFSTAmout(FSTREQ objRequest)
        {
            FSTRES OBJRES = new FSTRES();
            try
            {
                FSTCOMMON obj = new FSTCOMMON();
                OBJRES = obj.GetFSTAmout(objRequest);
            }
            catch (Exception ex)
            {

            }
            return OBJRES;
        }

        [HttpPost, ActionName("Get_Transaction")]
        public Transaction_Webhook_Response Get_TransactionDetail(Transaction_Webhook objRequest)
        {
            Transaction_Webhook_Response OBJRES = new Transaction_Webhook_Response();
            try
            {
                FSTCOMMON obj = new FSTCOMMON();
                OBJRES = obj.Get_TransactionDetail(objRequest);
            }
            catch (Exception ex)
            {

            }
            return OBJRES;
        }
    }
}
