﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace STDFSTSHARED
{
    [DataContract(Name = "Transaction")]
    public class REQTransaction
    {
        [DataMember]
        public string outlet_pan { get; set; }
    }
}
