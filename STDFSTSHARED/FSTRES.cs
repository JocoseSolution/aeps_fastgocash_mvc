﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace STDFSTSHARED
{
    [DataContract]
    public class FSTRES
    {
        [DataMember]
        public string response_code { get; set; }
        [DataMember]
        public string response_msg { get; set; }
        [DataMember]
        public List<ResTransaction> transactions { get; set; }
    }
}
