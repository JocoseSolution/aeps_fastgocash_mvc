﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace STDFSTSHARED
{
    [DataContract(Name = "Transaction")]
    public class ResTransaction
    {
        [DataMember]
        public string balance { get; set; }
    }
}
