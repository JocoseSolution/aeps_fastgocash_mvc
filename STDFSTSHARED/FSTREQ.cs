﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace STDFSTSHARED
{
    [DataContract]
    public class FSTREQ
    {
        [DataMember]
        public string request_id { get; set; }
        [DataMember]

        public string session_id { get; set; }
        [DataMember]
        public string app_id { get; set; }
        [DataMember]
        public string outlet_pan { get; set; }
        [DataMember]
        public List<REQTransaction> transactions { get; set; }

        [DataMember]
        public string hash { get; set; }

    }

    #region [Transaction Webhook]
    public class Transaction
    {
        public string sp_key { get; set; }
        public string amount { get; set; }
        public List<string> customer_params { get; set; }
    }

    public class Transaction_Webhook
    {
        public string request_id { get; set; }
        public string session_id { get; set; }
        public string app_id { get; set; }
        public string outlet_pan { get; set; }
        public List<Transaction> transactions { get; set; }
        public string hash { get; set; }
    }

    public class Transaction_Response
    {
        public string agent_id { get; set; }
    }

    public class Transaction_Webhook_Response
    {
        public string response_code { get; set; }
        public string response_msg { get; set; }
        public List<Transaction_Response> transactions { get; set; }
    }

    #endregion

}
