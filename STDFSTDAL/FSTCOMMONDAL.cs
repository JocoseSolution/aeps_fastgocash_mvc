﻿using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using STDFSTSHARED;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STDFSTDAL
{
    public class FSTCOMMONDAL
    {
        private SqlDatabase DBHelper;
        public FSTCOMMONDAL()
        {
            DBHelper = new SqlDatabase(ConfigurationManager.ConnectionStrings["FSTGOB2B"].ConnectionString);//ConfigurationManager.ConnectionStrings["BigBreaksB2B"].ConnectionString);
        }
        public FSTRES GetFSTAmout(FSTREQ fstGo)
        {
            FSTRES objResponse = new FSTRES();
            DataSet ds = null;
            try
            {
                //SqlCommand cmd = new SqlCommand("flight.usp_APIAuthentication");  // Use FWSAPI Database for APIAuthentication  
                SqlCommand cmd = new SqlCommand("usp_GET_AgencyBalance");   // Use B2B FWS Database for APIAuthentication  
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@app_id", fstGo.app_id);
                cmd.Parameters.AddWithValue("@request_id", fstGo.request_id);
                cmd.Parameters.AddWithValue("@outlet_pan", fstGo.outlet_pan);
                ds = DBHelper.ExecuteDataSet(cmd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    DataRow dr = ds.Tables[0].Rows[0];
                    objResponse.transactions = new List<ResTransaction>();
                    objResponse.transactions.Add(new ResTransaction { balance = Convert.ToString(dr["Aval_Balance"]) });
                    objResponse.response_code = Convert.ToString(dr["response_code"]);
                    objResponse.response_msg = Convert.ToString(dr["response_msg"]);
                }
            }
            catch (Exception ex)
            {
                Exception exceptionToThrow;

            }
            return objResponse;
        }

        public Transaction_Webhook_Response Get_TransactionDetail(Transaction_Webhook fstGo)
        {
            Transaction_Webhook_Response objResponse = new Transaction_Webhook_Response();
            DataSet ds = null;
            try
            {
                //SqlCommand cmd = new SqlCommand("flight.usp_APIAuthentication");  // Use FWSAPI Database for APIAuthentication  
                SqlCommand cmd = new SqlCommand("usp_GET_AEPS_TransactionWebhook");   // Use B2B FWS Database for APIAuthentication  
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@mobile", fstGo.transactions[0].customer_params[0]);
                cmd.Parameters.AddWithValue("@panno", fstGo.outlet_pan);
                ds = DBHelper.ExecuteDataSet(cmd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    DataRow dr = ds.Tables[0].Rows[0];
                    objResponse.transactions = new List<Transaction_Response>();
                    objResponse.transactions.Add(new Transaction_Response { agent_id = Convert.ToString(dr["agent_id"]) });
                    objResponse.response_code = Convert.ToString(dr["response_code"]);
                    objResponse.response_msg = Convert.ToString(dr["response_msg"]);
                }
            }
            catch (Exception ex)
            {
                Exception exceptionToThrow;

            }
            return objResponse;
        }
    }
}
